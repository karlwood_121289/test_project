﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf;

namespace Practise_application
{
    class Program
    {
        static void Main(string[] args)
        {

            //This might need to be file handle which is created using csv handle etc.
            //Then we make file handle with switch statement which uses parse file? check dynamic loading?

            //Example();
            var file_lines = FileOperations.ReadFromFile("C:\\Users\\Karl\\Desktop\\wrk\\programming\\Book1.csv");

            CSV_handler file_handle = new CSV_handler();

            var data_to_send = file_handle.parse_file(file_lines);

            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[0];
            
            SocketClient.SendData(ipAddress, data_to_send.ToByteArray());

            //var file_contents = FileOperations.ReadFromFile(".test_file.txt");
            //Console.WriteLine(file_contents);

        }


        static void Example()
        {
            //This could be 2 seperate apps communicating. alternatively this could be replaced with a process to launch a web server.
            try
            {
                using (Process server_process = new Process())
                {
                    //server_process.StartInfo.UseShellExecute = false;
                    server_process.StartInfo.FileName = "C:\\Users\\Karl\\source\\repos\\Practise application\\Listener\\bin\\Debug\\Listener.exe";
                    server_process.Start();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[0];

            //SocketClient.Example(ipAddress, "test data");
            //WebInterface.example();
        }

    }
}
